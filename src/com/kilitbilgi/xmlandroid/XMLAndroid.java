package com.kilitbilgi.xmlandroid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.TextView;

@SuppressLint("NewApi")
public class XMLAndroid extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_xmlandroid);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		String path = "http://www.ahmettalut.com/pizza.xml";

		xmlTest xml = new xmlTest();
		xml.startXML(path);

		String allInfo_ = xml.getAllInfo();

		// XML Kaynagini buradan degistirebilirsiniz.

		TextView xmlInfoTxt = (TextView) findViewById(R.id.xmlInfoTxt);

		xmlInfoTxt.setText(allInfo_);
	}
}
